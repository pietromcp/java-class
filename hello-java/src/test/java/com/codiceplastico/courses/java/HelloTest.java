package com.codiceplastico.courses.java;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsEqual.equalTo;
import static org.junit.Assert.assertThat;
import org.junit.Test;

public class HelloTest {
    @Test
    public void shouldSayHelloWorld() {
        assertThat(new Hello().sayHello(), is(equalTo("Hello, World!")));
    }
}
