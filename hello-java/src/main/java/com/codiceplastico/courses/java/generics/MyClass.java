package com.codiceplastico.courses.java.generics;

public class MyClass<T> {

}

class A {
}

interface B {
}

interface C {
}

class D<T extends A & B & C> {

}


// Compilation error
//class E<T extends B & C & A> {
//}

class F<T1 extends B, T2 extends T1> {

}

class AnotherClass {
    <T1, T2> void AMethod(T1 x, T2 y) {

    }
}

interface Polygon {
}

interface Builder<T extends Polygon & Comparable<T>> {
    T build();
}

class Square implements Polygon, Comparable<Square> {
    public Square(int width) {

    }

    @Override
    public int compareTo(Square o) {
        return 0;
    }
}

class Rectangle implements Polygon, Comparable<Rectangle> {
    public Rectangle(int width, int height) {

    }

    @Override
    public int compareTo(Rectangle o) {
        return 0;
    }
}

abstract class BaseBuilder<T extends Polygon & Comparable<T>, TBuilder extends Builder<T>> implements Builder<T> {
    protected int width;

    public TBuilder withWidth(int w) {
        width = w;
        return (TBuilder)this;
    }
}

class SquareBuilder extends BaseBuilder<Square, SquareBuilder> {
    @Override
    public Square build() {
        return new Square(width);
    }
}

class RectangleBuilder extends BaseBuilder<Rectangle, RectangleBuilder> {
    protected int height;

    public RectangleBuilder withHeight(int h) {
        height = h;
        return this;
    }

    @Override
    public Rectangle build() {
        return new Rectangle(width, height);
    }
}

class BuildersMain {
    public static void main(String[] args) {
        Rectangle r = new RectangleBuilder().withWidth(10).withHeight(20).build();
        Square s = new SquareBuilder().withWidth(11).build();
    }
}

class Utils {
    public static <T, TDefault extends T> T valueOrDefault(T value, TDefault defaultValue) {
        return value != null ? value : defaultValue;
    }
}