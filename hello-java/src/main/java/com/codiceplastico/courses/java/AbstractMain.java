package com.codiceplastico.courses.java;

public class AbstractMain {
    public static void main(String[] args) {
        //Parent p0 = new Parent();
        Parent p1 = new Child();
    }
}

interface Parent {
    public abstract String getValue();
}

class Child implements Parent {
    @Override
    public String getValue() {
        return null;
    }

    @Override
    public String toString() {
        return super.toString();
    }
}

interface Interface1 {
    String toString();
}

interface Interface2 {
    String toString();
}

class MyClass0 implements Interface1, Interface2 {

}

abstract class MyStringableClass {
    public abstract String toString();
}

class MyClass1 {
    public final void foo() {
        int age = 40;
        age = 41;
        final int x = 19;
        //x = 11;
    }
}

class MyClass2 extends MyClass1 {
//    @Override
//    public void foo() {
//
//    }
}