package com.codiceplastico.courses.java.coins.byhand;

import java.math.BigDecimal;

public abstract class EuroCoin {
    public static EuroCoin Two = new TwoEuroCoin();
    public static EuroCoin One = new OneEuroCoin();
    public static EuroCoin FiftyCents = new FiftyCentsEuroCoin();

    private BigDecimal value;

    protected EuroCoin(BigDecimal value) {
        this.value = value;
    }

    public BigDecimal getValue() {
        return value;
    }

    public abstract String toString();
}

class TwoEuroCoin extends EuroCoin {
    TwoEuroCoin() {
        super(BigDecimal.valueOf(2));
    }

    @Override
    public String toString() {
        return "Two";
    }
}

class OneEuroCoin extends EuroCoin {
    OneEuroCoin() {
        super(BigDecimal.valueOf(1));
    }

    @Override
    public String toString() {
        return "One";
    }
}

class FiftyCentsEuroCoin extends EuroCoin {
    FiftyCentsEuroCoin() {
        super(BigDecimal.valueOf(0.5));
    }

    @Override
    public String toString() {
        return "FiftyCents";
    }
}