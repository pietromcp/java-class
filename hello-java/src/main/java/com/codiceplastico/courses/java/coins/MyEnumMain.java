package com.codiceplastico.courses.java.coins;

public class MyEnumMain {
    public static void main(String[] args) {
        doSomethingOnMyEnum(MyEnum.One);
        doSomethingOnMyEnum(MyEnum.Two);
        doSomethingOnMyEnum(MyEnum.Three);
    }

    private static void doSomethingOnMyEnum(MyEnum input) {
        input.doSomething();
    }
}
