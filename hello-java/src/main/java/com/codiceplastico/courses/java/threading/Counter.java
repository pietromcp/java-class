package com.codiceplastico.courses.java.threading;

public interface Counter {
    public int getValue();

    public void increment();
}
