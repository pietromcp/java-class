package com.codiceplastico.courses.java.coins;

public enum MyEnum {
    One {
        public void doSomething() {
            System.out.println("One.doSomething");
        }
    }, Two {
        public void doSomething() {
            System.out.println("Two.doSomething");
        }
    }, Three {
        public void doSomething() {
            System.out.println("Three.doSomething");
        }
    };

    public abstract void doSomething();
}
