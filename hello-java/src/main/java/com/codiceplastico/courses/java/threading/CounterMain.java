package com.codiceplastico.courses.java.threading;

import java.util.Random;

public class CounterMain {
    private static final int COUNTER_NUMBER = 50;
    public static void main(String[] args) throws InterruptedException {
        Counter counter = new NotThreadSafeCounter();
        runThreadsOnCounter(counter);
        runThreadsOnCounter(new ThreadSafeCounter());
        runThreadsOnCounter(new ThreadSafeCounterV2());
    }

    private static void runThreadsOnCounter(Counter counter) throws InterruptedException {
        Thread[] runners = new Thread[COUNTER_NUMBER];
        for(int i = 0; i < COUNTER_NUMBER; i++) {
            runners[i] = new Thread(new CounterRunner(counter));
            runners[i].start();
        }
        Thread.sleep(2000);
        System.out.printf("Expected: %d Actual: %d\n", COUNTER_NUMBER * CounterRunner.INCREMENTS, counter.getValue());
    }

    private static final class CounterRunner implements Runnable {
        private static final Random random = new Random();
        public static final int INCREMENTS = 100;
        private final Counter counter;

        private CounterRunner(Counter counter) {
            this.counter = counter;
        }

        @Override
        public void run() {
            try {
                Thread.sleep(random.nextInt(400));
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            for(int i = 0; i < INCREMENTS; i++) {
                counter.increment();
            }
        }
    }
}
