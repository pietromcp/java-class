package com.codiceplastico.courses.java.threading;

public class NotThreadSafeCounter implements Counter {
    private int count;

    public NotThreadSafeCounter() {
        count = 0;
    }

    @Override
    public int getValue() {
        return count;
    }

    @Override
    public void increment() {
        count++;
    }
}
