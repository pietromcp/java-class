package com.codiceplastico.courses.java;

public class PrimitiveTypeClassesMain {
    public static void main(String[] args) {
        System.out.println(int.class);
        System.out.println(Integer.class);
        Integer x = Integer.valueOf(19);
        int y = 19;
        System.out.println(x.getClass());
        //System.out.println(y.getClass()); // Compilation error
    }
}
