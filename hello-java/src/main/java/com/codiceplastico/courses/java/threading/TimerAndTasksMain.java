package com.codiceplastico.courses.java.threading;

import java.util.Timer;
import java.util.TimerTask;

public class TimerAndTasksMain {
    public static void main(String[] args) throws InterruptedException {
        TimerTask task = new MyTimerTask();
        Timer timer = new Timer(true);
        //timer.scheduleAtFixedRate(task, 0, 3 * 1000);
        //timer.schedule(task, 250, 2000);
        timer.schedule(task, 0);
        System.out.println("TimerTask started");
       //Thread.sleep(10000);
    }

    private static final class MyTimerTask extends TimerTask {
        @Override
        public void run() {
            System.out.println("MyTimerTask: start");
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("MyTimerTask: stop");
        }
    }
}
