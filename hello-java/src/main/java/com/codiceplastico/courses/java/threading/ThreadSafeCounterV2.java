package com.codiceplastico.courses.java.threading;

public class ThreadSafeCounterV2 implements Counter {
    private int count;

    public ThreadSafeCounterV2() {
        count = 0;
    }

    @Override
    public int getValue() {
        return count;
    }

    @Override
    public void increment() {
        synchronized (this) {
            count++;
        }
    }
}
