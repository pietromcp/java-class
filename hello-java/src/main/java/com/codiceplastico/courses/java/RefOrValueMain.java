package com.codiceplastico.courses.java;

public class RefOrValueMain {
    public static void main(String[] args) {
        String str = "xyz";
        aMethod(str);
        System.out.println(str);
    }

    private static void aMethod(String s) {
        System.out.println(s);
        s = "abc";
        System.out.println(s);
    }
}
