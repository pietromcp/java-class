package com.codiceplastico.courses.java.threading;

import java.io.IOException;
import java.util.Random;

public class ThreadingMain {
    private static final Random rnd = new Random();

    public static void main(String[] args) throws IOException, InterruptedException {
        Thread t0 = new MyThread("T0");

        Thread t1 = new MyThread("T1");

        Thread t2 = new Thread(() -> {
            printCurrentThreadInfo("T3");
            for (int i = 0; i < 15; i++) {
                try {
                    Thread.sleep(rnd.nextInt(100));
                    System.out.println("T3 " + i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });

        Thread t3 = new Thread(ThreadingMain::run);

        Thread t4 = new Thread(new MyThreadSupport("TS4")::execute);

        t0.start();
        t1.start();
        t2.start();
        t3.start();
        t4.start();

        Thread.sleep(500);
        System.out.println("Main thread completed");
    }

    private static void printCurrentThreadInfo(String name) {
        Thread currentThread = Thread.currentThread();
        System.out.printf("%s %s %s %s %s\n", name, currentThread.getName(), currentThread.getState(), currentThread.getThreadGroup().getName(), currentThread.getId());
    }

    private static void run() {
        printCurrentThreadInfo("RR");
        for (int i = 0; i < 15; i++) {
            try {
                Thread.sleep(rnd.nextInt(100));
                System.out.println("RR " + i);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static final class MyThread extends Thread {
        private final String name;

        private MyThread(String name) {
            this.name = name;
        }

        @Override
        public void run() {
            printCurrentThreadInfo(name);
            for (int i = 0; i < 15; i++) {
                try {
                    Thread.sleep(rnd.nextInt(100));
                    System.out.printf("%s &d\n", name, i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private static final class MyThreadSupport {
        private final String name;

        private MyThreadSupport(String name) {
            this.name = name;
        }

        public void execute() {
            printCurrentThreadInfo(name);
            for (int i = 0; i < 15; i++) {
                try {
                    Thread.sleep(rnd.nextInt(100));
                    System.out.printf("%s %d\n", name, i);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

        }
    }
}
