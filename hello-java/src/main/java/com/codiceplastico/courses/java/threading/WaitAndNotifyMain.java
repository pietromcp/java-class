package com.codiceplastico.courses.java.threading;

public class WaitAndNotifyMain {

    public static void main(String[] args) throws InterruptedException {
        Object monitor = new Object();

        Thread t1 = new Thread(() -> {
            System.out.println("Thread 1 - START and WAIT");
            try {
                synchronized (monitor) {
                    monitor.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread 1 - Completed");
        });

        Thread t2 = new Thread(() -> {
            System.out.println("Thread 2 - START and WAIT");
            try {
                synchronized (monitor) {
                    monitor.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread 2 - Completed");
        });

        Thread t3 = new Thread(() -> {
            System.out.println("Thread 3 - START and WAIT");
            try {
                synchronized (monitor) {
                    monitor.wait();
                }
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            System.out.println("Thread 3 - Completed");
        });

        System.out.println("Main Thread: START");
        t1.start();
        t2.start();
        t3.start();
        Thread.sleep(1000);
        synchronized (monitor) {
            monitor.notify();
        }
        Thread.sleep(2400);
        synchronized (monitor) {
            monitor.notifyAll();
        }
        Thread.sleep(200);
        System.out.println("Main Thread: Completed");
    }
}
